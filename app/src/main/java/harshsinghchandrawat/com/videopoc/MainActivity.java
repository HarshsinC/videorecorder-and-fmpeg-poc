package harshsinghchandrawat.com.videopoc;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {


    private static final String TAG = "MainActivity";
    private FrameLayout mframeLayout;
    private ShowCamera mPreview;
    private Camera mCamera;
    private MediaRecorder mediaRecorder;
    private boolean mResult;
    FFmpeg ffmpeg;
    private ProgressDialog mProgressDialog;
    private Button mDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkAndRequestPermissions();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        creatPath();
        initFFMpeg();
        Button record = (Button) findViewById(R.id.capture);
        record.setOnTouchListener(this);
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        mDone = (Button) findViewById(R.id.done);
        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mergeVideos();
            }
        });


    }

    private void initFFMpeg() {
        try {
            if (ffmpeg == null) {
                Log.d("MainActivity", "ffmpeg : null");
                ffmpeg = FFmpeg.getInstance(this);
            }
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
//                    showUnsupportedExceptionDialog();
                }

                @Override
                public void onSuccess() {
                    Log.d("MainActivity", "ffmpeg : correct Loaded");
                }
            });
        } catch (FFmpegNotSupportedException e) {
//            showUnsupportedExceptionDialog();
        } catch (Exception e) {
            Log.d("MainActivity", "EXception not supported : " + e);
        }
    }

    private void execFFmpegBinary(final String[] command, final String outputFile) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    mResult = false;
                    Log.d(TAG, "FAILED with output : " + s);
                }

                @Override
                public void onSuccess(String s) {
                    mResult = true;
                    Log.d(TAG, "SUCCESS with output : " + s);
                }


                @Override
                public void onProgress(String s) {
                    Log.d(TAG, "progress : " + s);
                }

                @Override
                public void onStart() {
                    Log.d(TAG, "Started command :ffmpeg " + command);
                }

                @Override
                public void onFinish() {
                    Log.d(TAG, "Finished command :ffmpeg " + command);
                    if (!MainActivity.this.isFinishing()) {
                        if (mProgressDialog != null && mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                        if (mResult) {
                            deleteVideos();
                            if (outputFile != null && !outputFile.equals("")) {
                                mergeAudio(outputFile);
                            } else {
                                deleteVideosTemp();
                                Toast.makeText(MainActivity.this, "SUCCESS", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(MainActivity.this, "FAILED", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            });
        } catch (FFmpegCommandAlreadyRunningException e) {

        }
    }

    private void mergeAudio(String videoPath) {
//        ffmpeg -i video.avi -i audio.mp3 -codec copy -shortest output.avi
        String outPut = Environment.getExternalStorageDirectory().getAbsoluteFile() + "/VideoPOC/myvideos/";
        String audioPath = getAudioFile();
        String[] aa;
        if (audioPath != null && !audioPath.equals("")) {

            aa = new String[]{"-i", videoPath, "-i", audioPath, "-codec", "copy", "-shortest", outPut + getName() + ".mp4"};
        } else {
            aa = new String[]{"-i", videoPath, "-i", audioPath, "-codec", "copy", "-shortest", ""};
        }
        execFFmpegBinary(aa, "");
    }

    private String getAudioFile() {
        String path = Environment.getExternalStorageDirectory().getAbsoluteFile() + "/Download/";
        File directory = new File(path);
        File[] files = directory.listFiles();
        for (File file : files) {
            Uri uri = Uri.fromFile(file);
            String fileExt = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
            if (fileExt.equalsIgnoreCase("mp3")) {
                return file.getAbsolutePath();
            }
        }
        return "";
    }

    private void deleteVideos() {
        String path = Environment.getExternalStorageDirectory().getAbsoluteFile() + "/VideoPOC/clips/";
        File directory = new File(path);
        File[] files = directory.listFiles();
        for (File file : files) {
            file.delete();
        }
    }

    private void deleteVideosTemp() {
        String path = Environment.getExternalStorageDirectory().getAbsoluteFile() + "/VideoPOC/temp/";
        File directory = new File(path);
        File[] files = directory.listFiles();
        for (File file : files) {
            file.delete();
        }
    }

    public void concatenate(File[] file, String outputFile) {
        String list[] = generateList(file, outputFile);
        Log.d(TAG, "Concatenating " + list.toString());
        /*execFFmpegBinary(new String[] {
                "-i", inputFile1, "-i", inputFile2, "-filter_complex", "concat=n=2:v=1:a=1", "-f", "MP4", outputFile
        });*/
        execFFmpegBinary(list, outputFile);
    }


    private String[] generateList(File[] inputs, String outputFile) {
//  "-i", inputFile1, "-i", inputFile2, "-filter_complex", "concat=n=2:v=1:a=1", "-f", "MP4", outputFile
        String url[];
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < inputs.length; i++) {
            list.add("-i");
            list.add(inputs[i].getAbsolutePath());
        }
        list.add("-filter_complex");
        list.add("concat=n=" + inputs.length + ":v=1:a=1");
        list.add("-f");
        list.add("MP4");
        list.add(outputFile);
        url = list.toArray(new String[list.size()]);
        return url;
    }

    private void creatPath() {
        File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile(), "/VideoPOC/clips");
        File file2 = new File(Environment.getExternalStorageDirectory().getAbsoluteFile(), "/VideoPOC/temp");
        if (!file.exists()) {
            file.mkdirs();
        }

        if (!file2.exists()) {
            file2.mkdirs();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initialize();

    }

    public void initialize() {
        mCamera = Camera.open();
        mPreview = new ShowCamera(MainActivity.this, mCamera);
        mframeLayout = (FrameLayout) findViewById(R.id.framelayout);
        mframeLayout.addView(mPreview);
    }


    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        int storage = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int loc = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }
        if (storage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (loc != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.RECORD_AUDIO);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), 0);
            return false;
        }
        return true;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                getName();
                Toast.makeText(MainActivity.this, "start", Toast.LENGTH_SHORT).show();
                mDone.setEnabled(false);
                prepareMediaRecorder();
                try {
                    mediaRecorder.start();
                } catch (final Exception ex) {
                }
                return true;
            case MotionEvent.ACTION_UP:
                Toast.makeText(MainActivity.this, "stop", Toast.LENGTH_SHORT).show();
                mDone.setEnabled(true);
                try {
                    mediaRecorder.stop();
                    releaseMediaRecorder();
                } catch (RuntimeException stopException) {
                }
                return true;
        }
        return false;
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();
            mediaRecorder.release();
            mediaRecorder = null;
            mCamera.lock();
        }
    }

    private boolean prepareMediaRecorder() {

        mediaRecorder = new MediaRecorder();

        mCamera.unlock();
        mediaRecorder.setCamera(mCamera);

        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));

        mediaRecorder.setOutputFile(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/VideoPOC/clips/" + getName() + ".3gp");
//        mediaRecorder.setMaxDuration(6000000); // Set max duration 60 sec.
//        mediaRecorder.setMaxFileSize(50000000); // Set max file size 50M

        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            releaseMediaRecorder();
            return false;
        }
        return true;

    }

    private String getName() {
        Date currentTime = Calendar.getInstance().getTime();
        Log.d(TAG, "file name: " + currentTime.toString());
        return currentTime.toString();
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();
        deleteVideos();
    }


    private void mergeVideos() {
        String mFileName = Environment.getExternalStorageDirectory().getAbsoluteFile() + "/VideoPOC/temp/";
        String path = Environment.getExternalStorageDirectory().getAbsoluteFile() + "/VideoPOC/clips/";
        Log.d("MainActivity", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
        if (files.length > 1) {
            mProgressDialog = ProgressDialog.show(MainActivity.this,
                    "Please wait", "Preparing video...", true);
            concatenate(files, mFileName + "/" + getName() + ".mp4");
        }
    }
}
